import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LedModule } from './led/led.module';
import { WebsocketModule } from 'led/websockets/websockets.module';

@Module({
  imports: [LedModule,
   WebsocketModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
