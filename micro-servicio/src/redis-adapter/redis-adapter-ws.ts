import { IoAdapter } from '@nestjs/websockets';
import * as redisIoAdapter from "socket.io-redis";

const redisAdapter = redisIoAdapter({ host: '192.168.20.108', port: 32768 });

export class RedisIoAdapter extends IoAdapter {
  createIOServer(port: number, options?: any): any {
    const server = super.createIOServer(port, options);
    server.adapter(redisAdapter);
    return server;
  }
}
