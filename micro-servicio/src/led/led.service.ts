import { Injectable } from "@nestjs/common";
//import { Gpio } from 'onoff';
import {Gpio} from 'pigpio'

@Injectable()
export class LedService {
    private _led = new Gpio(4,{mode: Gpio.OUTPUT});
    private _trigger = new Gpio(17,{mode: Gpio.OUTPUT});
    private _echo=new Gpio(18,{mode: Gpio.OUTPUT, alert: true})
    private  MICROSEGUNDOS_POR_CM = 1e6/34321;

    constructor(
                
        ) {     }

    async encenderApagarLed(estadoLed) { 
    
        if (estadoLed === 0) {
            console.log('El LED esta apagado');
            return await this._led.digitalWrite(0);
        } else {
            console.log('El LED esta prendido');
            return  await this._led.digitalWrite(1);
        }

    } 
}
