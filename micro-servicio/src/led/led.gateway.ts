import { WebSocketGateway, OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage } from "@nestjs/websockets";
import { LedService } from "./led.service";
import { from, Observable } from "rxjs";

@WebSocketGateway(3001)
export class LedGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    constructor(
        private readonly _ledService: LedService
    ) { }
    afterInit(server: any) {
        console.log('Init led-escucha');

    }
    handleConnection(client: any, ...args: any[]) {
        console.log('conectado', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('se desconecto:', client.id);
    }

    @SubscribeMessage('encenderLed')
    encenderLed(client, data) {
        console.log('Entro a led gateway', client.id);
        console.log('data al servicio',data);
        const encenderApagarLed = this._ledService.encenderApagarLed(data.estado)
        return encenderApagarLed;
    }

}

