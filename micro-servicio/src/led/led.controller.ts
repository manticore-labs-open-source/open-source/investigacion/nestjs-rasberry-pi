import { Controller } from '@nestjs/common';
import { LedService } from './led.service';
import { LedUpdateDto } from './led-update-dto/led-update-dto';
import { LedCreateDto } from './led-create-dto/led-create-dto';
import { politicasLed } from './led-politicas/led.politicas';
import { mensajesLed } from './led-mensajes/led.mensajes';
import { PrincipalController } from '@manticore-labs/nest';

@Controller('led')
export class LedController extends PrincipalController<LedCreateDto, LedUpdateDto> {    constructor(private readonly _ledService: LedService) {
        super( politicasLed, // politicas de seguridad
        _ledService, //servicio
            { //Dto
                CreateDto: LedCreateDto,
                UpdateDto: LedUpdateDto
            },
            0, //skip
            30, //take
            mensajesLed,
            undefined // contexto
            );
            this.contexto = this;
    }

}

