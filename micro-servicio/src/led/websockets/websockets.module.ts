
import { Module } from "@nestjs/common";
import { LedGateway } from "led/led.gateway";
import { LedModule } from "led/led.module";

@Module({
    providers:[LedGateway],
    imports:[LedModule]

})
export class WebsocketModule{
    
}
