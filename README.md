# Nest.js + Rasberry Pi 

  

## Indice 

1. [Introduccion](#id1) 
2. [Configuracion y Instalacion](#id2) 
3. [Implementacion](#id3)  

  

  

<a name="id1"></a> 

## Introduccion  
**¿Qué es la Raspberry Pi?**
La Raspberry Pi es una computadora pequeña, asequible y con una capacidad asombrosa para tarjetas de crédito.
Está desarrollado por la Fundación Raspberry Pi y podría ser la tecnología más versátil jamás creada.
El objetivo del creador Eben Upton era crear un dispositivo de bajo costo que mejorara las habilidades de programación y la comprensión del hardware.
Debido al pequeño tamaño y al precio del dispositivo, se ha convertido en el centro de una amplia gama de proyectos por parte de pequeños, fabricantes y entusiastas de la electrónica.

**Raspberry Pi y Node.js**
La Raspberry Pi tiene una fila de pines GPIO (entrada / salida de uso general), y se pueden utilizar para interactuar de forma sorprendente con el mundo real. Este tutorial se centrará en cómo usarlos con el framework Nest.js.

**¿Qué es GPIO?**
GPIO significa salida de propósito general.

La Raspberry Pi tiene dos filas de pines GPIO, que son conexiones entre la Raspberry Pi y el mundo real.

Los pines de salida son como los interruptores que la Raspberry Pi puede encender o apagar (como encender / apagar una luz LED). Pero también puede enviar una señal a otro dispositivo.

Los pines de entrada son como los interruptores que puede activar o desactivar desde el mundo exterior (como un interruptor de luz de encendido / apagado). Pero también puede ser un dato de un sensor o una señal de otro dispositivo.

¡Eso significa que puede interactuar con el mundo real y controlar dispositivos y dispositivos electrónicos utilizando la Raspberry PI y sus pines GPIO!
<a name="id2"></a> 

## Configuracion y Instalacion
Materiales:

Una Raspberry Pi con Raspian, * * Internet, SSH, con Node.js instalado
El módulo pigpio para Node.js.
El módulo socket.io para Node.js
1 x Breadboard
3 x 220 ohmios de resistencia
1 x LED RGB (ánodo común o  *   * cátodo común)
4 x hembra a cables de puente macho
#### Librería pigpio 
>Instalación 
1. Instalación de la libreria en C de gpio
~~~
sudo apt-get update
sudo apt-get install pigpio
~~~
2. Instalación del paquete pigpio
~~~
npm install pigpio
~~~

## Instalación de las libreria  pigpio en nest

**Escribir en la linea de comandos las siguientes lineas**

```
rm pigpio.zip
sudo rm -rf PIGPIO
wget abyz.me.uk/rpi/pigpio/pigpio.zip
unzip pigpio.zip
cd PIGPIO
make
sudo make install
sudo apt install gcc 
sudo apt install build-essential

```

Para utilizar en el nest con types script se instaló de la siguiente manera: 

```
npm install @types/pigpio
npm install @types/onoff 
```
  <a name="id3"></a> 
  ## Implementacion
  Se utilizara sockets para su implementacion:
  * Instalar las siguientes dependencias:
  ```
npm install @types/socket.io-redis
npm i --save @nestjs/websockets
npm install --save express@4.15.2

```
  * Los parametros de entrada, son el estado 0 o 1 
* Salida,  Prendido o apagado del led
* Versiones:
  * node v8.11.3
  * nvm  0.33.11
  * npm  5.6.0
  * Python 2.7.15rc1 
  * gcc compiler version: 7.3 

> led.service.ts
```
import { Injectable } from "@nestjs/common";
//import { Gpio } from 'onoff';
import {Gpio} from 'pigpio'

@Injectable()
export class LedService {
    private _led = new Gpio(4,{mode: Gpio.OUTPUT});
    private _trigger = new Gpio(17,{mode: Gpio.OUTPUT});
    private _echo=new Gpio(18,{mode: Gpio.OUTPUT, alert: true})
    private  MICROSEGUNDOS_POR_CM = 1e6/34321;

    constructor(
                
        ) {     }

    async encenderApagarLed(estadoLed) { 
    
        if (estadoLed === 0) {
            console.log('El LED esta apagado');
            return await this._led.digitalWrite(0);
        } else {
            console.log('El LED esta prendido');
            return  await this._led.digitalWrite(1);
        }

    } 
}

```
> led.gateway.ts

```
import { WebSocketGateway, OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage } from "@nestjs/websockets";
import { LedService } from "./led.service";
import { from, Observable } from "rxjs";

@WebSocketGateway(3001)
export class LedGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    constructor(
        private readonly _ledService: LedService
    ) { }
    afterInit(server: any) {
        console.log('Init led-escucha');

    }
    handleConnection(client: any, ...args: any[]) {
        console.log('conectado', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('se desconecto:', client.id);
    }

    @SubscribeMessage('encenderLed')
    encenderLed(client, data) {
        console.log('Entro a led gateway', client.id);
        console.log('data al servicio',data);
        const encenderApagarLed = this._ledService.encenderApagarLed(data.estado)
        return encenderApagarLed;
    }

}

```
En la raiz de proyecto crear la carpeta public con un archivo html ***/public/index.html***
Instanciar en el main de la siguiente manera:
```
app.useStaticAssets(__dirname + '/../public');
```
> index.html
~~~~
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="socket.io.js"></script>

    <title>Document</title>
</head> 

<body>
    <h1>Cliente</h1>

    <script>
    //iniar socket
    socket= io('http://192.168.20.101:3001')



    //conectar el socket
    socket.on('connect',()=>{
    console.log('se conecto el socket cliente', socket.id)
    })
 
    function encenderLed(data){
        socket.emit('encenderLed',data,(respuesta)=>{
            console.log(respuesta);
        })
    }



    </script>
</body>

</html>
~~~~
**Nota:** No olvidar el archivo ***socket.io.js*** en la carpeta publica mas informacion en el repositorio
[aqui el source](https://gitlab.com/manticore-labs/open-source/investigacion/nestjs-rasberry-pi)
  
  

  

  

  

  

## Redes Sociales 

<a href="https://twitter.com/crisjc8" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @crisjc8 </a><br> 

<a href="https://linkedin.com/in/cristhian-jumbo-748934180/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Cristhian Jumbo</a><br> 

<a href="https://www.instagram.com/crisjc6/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> crisjc6</a><br> 

 )